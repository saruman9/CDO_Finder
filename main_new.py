__author__ = 'neo'
# -*- coding: utf-8 -*-
import urllib
import datetime
from time import sleep
import requests
import os
import sys


def resource_path(relative):
    return os.path.join(getattr(sys, '_MEIPASS', os.path.abspath(".")),
                        relative)


def ObtainDate():
    isValid = False
    while not isValid:
        userIn = raw_input("Input date and time (dd.mm.yy hh:mm): ")
        try:
            d = datetime.datetime.strptime(userIn, "%d.%m.%y %H:%M")
            return d
        except:
            print("Input correct date!\n")


def SearchDate(dDate):
    if dDate.year == 2012:
        return dDate.isocalendar()[1] - 14
    else:
        if dDate.month <= 2 and dDate.day <= 9:
            return dDate.isocalendar()[1] + 37
        else:
            return dDate.isocalendar()[1] + 38


def SearchFreePlace(iId, dDate):
    url = "http://de.ifmo.ru/index.php?node=" + str(iId)
    sHtml = str(urllib.urlopen(url).read())
    sDate = dDate.strftime('%d.%m.%Y %H:%M:%S')
    if sHtml[sHtml.find(sDate):sHtml.find(sDate) + 160].find('gray') == -1:
        iAllPlace = int(sHtml[sHtml.find(sDate) + 97:sHtml.find(sDate) + 99])
        try:
            iBusyPlace = int(sHtml[sHtml.find(sDate) + 125:sHtml.find(sDate) + 127])
        except:
            iBusyPlace = int(sHtml[sHtml.find(sDate) + 125:sHtml.find(sDate) + 126])
        iFreePlace = iAllPlace - iBusyPlace
        #print iAllPlace, iFreePlace
    else:
        return 0
    return iFreePlace


def Apply(dDate, iNumSes, sUser, sPass):
    cert_path = resource_path('cacert.pem')
    sDate = dDate.strftime('%d.%m.%Y+%H:%M:%S') + '&login=' + sUser + '&passwd=' + sPass + '&role=%D1%F2%F3%E4%E5%ED%F2'
    sUrl = "https://de.ifmo.ru/--schedule/index.php?data=" + sDate
    #print sUrl
    r = requests.get(sUrl, allow_redirects=False, verify=cert_path)
    #print r.url
    dCookies = dict(PHPSESSID=r.cookies['PHPSESSID'])
    #print dCookies
    dDateCpy = dDate + datetime.timedelta(minutes=40 * iNumSes)
    #print dDateCpy
    sUrl = 'https://de.ifmo.ru/--schedule/student.php?view=sr&month1=' + dDate.strftime('%m') + '&year1=' + dDate.strftime('%Y') + '&func=sr&data2=' + dDate.strftime('%d.%m.%Y') + '&begin1=' + dDate.strftime('%H:%M') + '&end1=' + dDateCpy.strftime('%H:%M')
    #print sUrl
    r = requests.get(sUrl, cookies=dCookies, allow_redirects=False, verify=cert_path)
    print 'Success!'
    print 'REMEMBER! You record on ' + dDate.strftime('%d %B, %A') + ' from ' + dDate.strftime('%H:%M') + ' to ' + dDateCpy.strftime('%H:%M')
    print 'About all bugs (including language) and suggestions report to skype:pr0_ps or ' \
            'e-mail:rum.274.4@gmail.com! Thank\'s!'


try:
    cert_path = resource_path('cacert.pem')
    dDateInp = ObtainDate()
    iDateInpNum = SearchDate(dDateInp)

    while not (21 < iDateInpNum <= 64 and dDateInp.date() > datetime.date.today()):
        print("Sorry, try again!\n")
        #print iDateInpNum
        #print dDateInp.isocalendar()[1]
        dDateInp = ObtainDate()
        iDateInpNum = SearchDate(dDateInp)

    isValid = False
    while not isValid:
        try:
            iNumSes = iNumSesCpy = int(raw_input("Input number of session: "))
            isValid = True
        except:
            print 'Input integer!\n'


    sAnswer = ''
    while sAnswer.lower() != 'y' and sAnswer.lower() != 'n':
        sAnswer = raw_input("Automatically login and apply? (y/n) ")
    if sAnswer.lower() == 'y':
        sLogin = raw_input("Input login: ")
        sPassword = raw_input("Input password: ")
        sDate = dDateInp.strftime('%d.%m.%Y+%H:%M:%S') + '&login=' + sLogin + '&passwd=' + sPassword + '&role=%D1%F2%F3%E4%E5%ED%F2'
        sUrl = "https://de.ifmo.ru/--schedule/index.php?data=" + sDate
        #print sUrl
        r = requests.get(sUrl, verify=cert_path)
        while r.url != 'https://de.ifmo.ru/--schedule/student.php':
            print 'Incorrect login and/or password! Try again.'
            sLogin = raw_input('Input login: ')
            sPassword = raw_input('Input password: ')
            sDate = dDateInp.strftime('%d.%m.%Y+%H:%M:%S') + '&login=' + sLogin + '&passwd=' + sPassword + \
                '&role=%D1%F2%F3%E4%E5%ED%F2'
            sUrl = "https://de.ifmo.ru/--schedule/index.php?data=" + sDate
            r = requests.get(sUrl, verify=cert_path)


    iFree = 0
    while iFree == 0 and iNumSes != 0:
        iFree = SearchFreePlace(iDateInpNum, dDateInp)
        if iFree > 0:
            print dDateInp, ' - is free!'
            iNumSes -= 1
            dDateInp = dDateInp + datetime.timedelta(minutes=40)
            iFree = 0
        else:
            print dDateInp, ' - is full!'
            sleep(5)
    dDateInp = dDateInp - datetime.timedelta(minutes=40 * iNumSesCpy)
    if sAnswer.lower() == 'y':
        Apply(dDateInp, iNumSesCpy, sLogin, sPassword)
    else:
        print 'Success! All place is free! Run to record!!!'
        print 'About all bugs (including language) and suggestions report to skype:pr0_ps or ' \
                'e-mail:rum.274.4@gmail.com! Thank\'s!'
except:
    print "Sorry! Try again, please. May be CDO does not work or incorrect input date. If errors output constantly that contact me!"
    print 'About all bugs (including language) and suggestions report to skype:pr0_ps or e-mail:rum.274.4@gmail.com!' \
          ' Thank\'s!'
